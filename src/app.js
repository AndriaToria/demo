// Import CSS and Images
const css = require('./css')
const images = require.context('./images', true)

//Import JS
import * as RequestHelper from './js/helpers/requestHelper'
import * as DashboardHelper from './js/helpers/dashboardHelper'
import * as LoginHelper from './js/helpers/loginHelper'
import * as Dashboard from './js/dashboard'
import * as Authentication from './js/authentication'
import * as Groups from './js/groups'
import * as Group from './js/group'
import * as Events from './js/events'
import * as Polls from './js/polls'
import * as Friends from './js/friends'
import * as User from './js/user'
import * as Users from './js/users'
import * as Profile from './js/profile'
import * as Notifications from './js/notifications'
import * as Messages from './js/messages'
window.RequestHelper = RequestHelper
window.DashboardHelper = DashboardHelper
window.LoginHelper = LoginHelper 
window.Dashboard = Dashboard
window.Authentication = Authentication
window.Groups = Groups
window.Group = Group
window.Events = Events;
window.Polls = Polls
window.Friends = Friends
window.User = User
window.Users = Users;
window.Profile = Profile
window.Notifications = Notifications
window.Messages = Messages

// Set navigo to access globally
const Navigo = require('navigo');
var router = new Navigo(null, true, '#!');
window.router = router


// Import templates
var auth = require('./templates/auth.js')
var dashboard = require('./templates/dashboard.js')
var groupList = require('./templates/group-list.js')
var events = require('./templates/events.js')
var eventForm = require('./templates/event-form.js')
var poll = require('./templates/poll.js')
var friends = require('./templates/friends.js')
var allUser = require('./templates/all-user.js')
var groupForm = require('./templates/group-form.js')
var group = require('./templates/group.js')
var event = require('./templates/event.js')
var editProfile = require('./templates/edit-profile')
var notifications = require('./templates/notifications')
var profile = require('./templates/profile')
var message = require('./templates/message')
var commonHeader = require('./templates/common-header')


var appEl = document.getElementById('app');
var headEl = document.getElementById('head');

window.onload = function(){
    router.on({
        'login': function () {
            appEl.innerHTML = auth;
            headEl.innerHTML = ''
            LoginHelper.checkAuth()
            document.getElementsByTagName("body")[0].style['background-color'] = "blue";
        },
        'dashboard': function () {
            appEl.innerHTML = dashboard;
            headEl.innerHTML = commonHeader
            document.getElementsByTagName("body")[0].style['background-color'] = "#e8e8e8";
            DashboardHelper.loggedUserCheck();
            Dashboard.loadDashboard();
        },
        'group-list': function () {
            appEl.innerHTML = groupList
            headEl.innerHTML = commonHeader
            DashboardHelper.loggedUserCheck();
            Groups.loadGroups();
        },
        'group-form': function(){
            appEl.innerHTML = groupForm
            headEl.innerHTML = commonHeader
            DashboardHelper.loggedUserCheck();

        },
        'group/:id': function(){
            appEl.innerHTML = group
            headEl.innerHTML = commonHeader
            DashboardHelper.loggedUserCheck();
            Group.loadGroupPosts();
        },
        'events': function(){
            appEl.innerHTML = events
            headEl.innerHTML = commonHeader
            DashboardHelper.loggedUserCheck();
            Events.loadEvents();
        },
        'event-form': function(){
            appEl.innerHTML = eventForm;
            headEl.innerHTML = commonHeader
            DashboardHelper.loggedUserCheck();
        },
        'event/:id':function(){
            appEl.innerHTML = event
            headEl.innerHTML = commonHeader
            DashboardHelper.loggedUserCheck();
            Events.loadEvent();
        },
        'polls': function(){
            appEl.innerHTML = poll;
            headEl.innerHTML = commonHeader
            DashboardHelper.loggedUserCheck();
            Polls.loadPolls();
        },
        'friends': function(){
            appEl.innerHTML = friends;
            headEl.innerHTML = commonHeader
            DashboardHelper.loggedUserCheck();
            Friends.loadFriendUsers();
        },
        'all-user': function(){
            appEl.innerHTML = allUser;
            headEl.innerHTML = commonHeader
            DashboardHelper.loggedUserCheck();
            Users.loadUsers();
        },
        'edit-profile': function(){
            appEl.innerHTML = editProfile
            headEl.innerHTML = commonHeader
            DashboardHelper.loggedUserCheck();
            Profile.setUserForm()
        },
        'notifications': function(){
            appEl.innerHTML = notifications;
            headEl.innerHTML = commonHeader
            DashboardHelper.loggedUserCheck();
            Notifications.loadNotifications();
        },
        'profile/:id':function(){
            appEl.innerHTML = profile;
            headEl.innerHTML = commonHeader
            document.getElementById('user_send_message').innerHTML= `
             <button onclick="Dashboard.sendMessage()" type="button" 
             class="button button-success user-action"> Send Message</button>`
            DashboardHelper.loggedUserCheck();
            Dashboard.loadDashboard()
        },
        'profile':function(){
            appEl.innerHTML = profile;
            headEl.innerHTML = commonHeader
            DashboardHelper.loggedUserCheck();
            Dashboard.loadDashboard()
        },
        'message/:id': function(){
            appEl.innerHTML = message;
            headEl.innerHTML = commonHeader
            DashboardHelper.loggedUserCheck();
            Messages.loadMessages()
        },
        '':function () {
            router.navigate('login')
        }
    })
    .resolve();

}
