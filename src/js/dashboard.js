

export var friends = [];
export var taggedUsers = [];
export var posts = [];
export var allUsers = [];
export var currentUser ={};


export function sendMessage(){
	var id = router._lastRouteResolved.params ? router._lastRouteResolved.params.id : null;

	router.navigate(`message/${id}`)

}

export function loadDashboard(){
	var id = router._lastRouteResolved.params ? router._lastRouteResolved.params.id : null;
	var user = JSON.parse(localStorage.getItem('user'));

	if(!id){
		id = user.id
	}
	var result =  RequestHelper.getRequest('http://localhost:3000/dashboard-data?id=' + id);
	allUsers = RequestHelper.getRequest('http://localhost:3000/all-users').data;
	if(!result.success){
		return alert('Something went wrong');
	}

	friends = result.data.friends;
	posts = result.data.posts;
	currentUser = result.data.user;

	Dashboard.displayBirthdays();
	Dashboard.displayInfo();
	Dashboard.displayTagFriends();
	Dashboard.displayPosts();
}

export function displayInfo(){


	if(document.getElementById('general-info')){
		document.getElementById('general-info').innerHTML = `
		<h3>${currentUser.name} ${currentUser.surname}</h3>
		<p><b>Email:</b> ${currentUser.email}</p>
		<p><b>Born:</b> ${currentUser.birthday ? currentUser.birthday : 'Not born yet' }</p>
		`
	}

	if(document.getElementById('user')){
		document.getElementById('user').innerHTML = `
		<h3>${currentUser.name} ${currentUser.surname}</h3>
		`
	}

}


export function displayTagFriends(){
	if(!document.getElementById('tags')){
		return;
	}

	var html = '';

	for(var friend of friends){
		html +=`
		<option value="${friend.id}">${friend.name} ${friend.surname}</option>
		`
	}
	document.getElementById('tags').innerHTML = html;


}

export function displayBirthdays(){
	if(!document.getElementById('birtday-body')){
		return;
	}

	var html = '';

	var counter = 0;
	for(var friend of friends){
		if(counter == 5){
			break;
		}

		if(friend.birthday){
			counter++;			
			html +=`
			<tr>
			<td>${friend.name} ${friend.surname}</td>
			<td>${friend.birthday}</td>
			</tr>
			`
		}
	}


	document.getElementById('birtday-body').innerHTML = html;
}


export function getTagedUsers(taggedUsers){
	taggedUsers = taggedUsers.split(',');
	var tagged = [];

	for (var taggedUserID of taggedUsers) {
		for (var user of allUsers) {
			if(user.id == taggedUserID){
				tagged.push(user.name + ' ' + user.surname);
			}
		}
	}

	return tagged.join(', ');
}

export function replyUserPost(post_id){
	var user = JSON.parse(localStorage.getItem('user'));

	var comment = document.getElementById(`comment-${post_id}`).value;

	var formData = {
		user_id: user.id,
		comment:comment,
		post_id:post_id
	}



	var result = RequestHelper.postRequest('http://localhost:3000/add-comment', formData);

	if(result.success){
		if(document.getElementById('tag-names')){
			document.getElementById('tag-names').innerHTML = 'Tags:';
		}
		taggedUsers = [];
		loadDashboard();
	}else{
		return alert('Error occured');
	}

}


export function displayPosts(){
	var html = '';



	for(var post of posts){
		//get tagged users
		var taggedUsers =null;
		if(post.taggedUsers){
			taggedUsers = getTagedUsers(post.taggedUsers)
		}


		html +=
		`
		<div class="post-section">
		<div class="author">
		<img src="images/user.png" class="post-img">
		</div>
		<div class="context">
		<div class="post-info">
		<p><b>${post.name} ${post.surname}</b>  / Posted: <i>${post.created_at}</i></p>
		</div>
		<div class="post-info">
		${post.content}
		<br>
		`
		if(taggedUsers){
			html +=`
			<p><b>Tags:</b> ${taggedUsers}  </p>

			`
		}
		html+=
		`
		</div>
		</div>
		<p style="margin-top: 5px"><b>Comments:</b></p>
		`;


		for (var comment of post.comments) {
			html +=`
			<div class="post-info comment">
			<div class="author" >
			<img src="images/user.png" class="post-img">
			</div>

			<p><b>${comment.name} ${comment.surname}</b> 
			${comment.comment}
			</p>
			</div>
			`;
		}

		html+=`
		<div class="post-info">
		<hr>
		<textarea
		id='comment-${post.id}' 
		style="width: 99%" 
		rows="2"
		class="textarea"
		placeholder="Reply Post"></textarea>
		<button onclick='Dashboard.replyUserPost(${post.id})' type="button" class="button button-primary reply-button">Reply</button>
		</div>
		</div>
		`
	}


	if(document.getElementById('posts')){
		
		document.getElementById('posts').innerHTML = html;
	}

}



export function addTagFunction () {
	//get select value
	var chosenID = document.getElementById('tags').value;
	//check if already tagged
	var exists = taggedUsers.find((elem) => elem.id == chosenID);
	if(!exists){
		//get user from friends
		var chosenUser = friends.find((elem) => elem.id == chosenID);
		//add to saved user
		taggedUsers.push(chosenUser);

		var names = taggedUsers.map((elem) => {
			return elem.name + ' ' + elem.surname
		})

		document.getElementById('tag-names').innerHTML = 'Tags: ' +  names.join(',');


	}
};


export function addPost() {
	var user = JSON.parse(localStorage.getItem('user'));
	var post = document.getElementById('post').value;
	var formData = {
		user_id: user.id,
		post: post,
	}

	if(taggedUsers.length > 0){
		formData.tagIDs = taggedUsers.map((elem) => elem.id).join(',');
	}



	var result = RequestHelper.postRequest('http://localhost:3000/add-post', formData);

	if(result.success){
		document.getElementById('tag-names').innerHTML = 'Tags:';
		document.getElementById('post').value = '';
		taggedUsers = [];
		loadDashboard();
	}else{
		return alert('Error occured');
	}


};

