export var users = [];
export var allUsers = [];

export function loadFriendUsers(){
	var user = JSON.parse(localStorage.getItem('user'));

	var result =  RequestHelper.getRequest('http://localhost:3000/get-friends?id=' + user.id);
	
	if(!result.success){
		return alert('Something went wrong');
	}

	users = result.data;
	allUsers = users;

	displayFriendUsers();


}


export function displayFriendUsers(){
	var html = '';
	for(var user of users){

		html += 
		`
		<div class="friend">
		<img src="images/cover.jpg" class="friend-cover">
		<img src="images/user.png" class="friend-profile-pic">

		<p style="padding: 5px"><b>${user.name} ${user.surname}</b></p>
		<button  onclick='User.deleteUser(${user.id})' type="button" class="button button-danger user-action delete-friend">Delete</button>
		<button onclick="router.navigate('profile/${user.id}')"  type="button" class="button button-primary user-action delete-friend"> Visit</button>
		</div>

		`
	}

	document.getElementById('friends').innerHTML = html;
	document.getElementById('total-friend').innerHTML = `<b>Total friends:</b> ${allUsers.length}`;
	
}

export function searchFriendUser(){
	var searchInput = document.getElementById('name').value;

	users = allUsers.filter((user) => {
		var fullName = user.name + ' ' + user.surname;
		return fullName.indexOf(searchInput) > -1
	})

	if(searchInput.length == 0){
		users = allUsers;
	}

	displayFriendUsers();

}


