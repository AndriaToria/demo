export var polls = [];

export function loadPolls(){
	var result =  RequestHelper.getRequest('http://localhost:3000/get-polls');
	
	if(!result.success){
		return alert('Something went wrong');
	}

	polls = result.data;

	var html = '';



	for(var poll of polls){

		html += 
		`
		<div class="post-section" id='poll-${poll.id}'>
		<div class="post-info">
		<p><b>${poll.question}</b></p>
		</div>
		<div class="post-info">
		<input type="radio" name="answer${poll.id}" value="${poll.answer1}">${poll.answer1}
		<br>
		<input type="radio" name="answer${poll.id}" value="${poll.answer2}">${poll.answer2}
		<br>
		<button onclick="Polls.vote(${poll.id}, '${poll.question}')" type="button" style="margin-top: 10px; float: right;"
		class="button button-success">Vote</button>

		</div>
		</div>
		`
	}

	document.getElementById('all-poll').innerHTML = html;

}


export function vote(id,question){
	//save vote
	var answers = document.getElementsByName('answer' + id);
	var answer = null;
	for(var i = 0; i < answers.length; i++){
		if(answers[i].checked){
			answer = answers[i].value;
		}
	}

	if(!answer){
		return alert('Choose question');
	}

	var formData = {
		poll_id: id,
		answer: answer
	}

	var result = RequestHelper.postRequest('http://localhost:3000/answer-poll', formData);

	if(!result.success){
		return alert('Something went wrong');
	}


	//get stats for vote
	var stats = result.stats;
	var currentPoll = polls.find((elem) => elem.id == id);

	var data = {};
	data[currentPoll.answer1] = 0;
	data[currentPoll.answer2] = 0;

	for (var stat of stats) {
		data[stat.answer] = stat.total;
	}

	//update vote section
	var html = `
	<div class="post-info">
	<p>Stats for: <b>${question}</b></p>
	</div>
	<div class="post-info">
	`;


	for(var answer in data){
		html += 
		`
		<p><b>${data[answer]}</b> people answered</p>
		<p>${answer}</p>
		`;
	}

	html += `</div>`;

	
	document.getElementById(`poll-${id}`).innerHTML = html;


}




export function addPoll() {
	var form = document.forms["pollForm"];

	var formData = {
		question:  form['question'].value,
		answer1: form['answer1'].value,
		answer2: form['answer2'].value,
	}

	for(var key in formData){
		var value = formData[key];
		if(value.length == 0){
			return alert('Please fill fields');
		}
	}
	
	var result = RequestHelper.postRequest('http://localhost:3000/add-poll', formData);

	if(result.success){
		loadPolls();
		return alert('Poll created');
	}else{
		return alert('Error occured');
	}
	

};
