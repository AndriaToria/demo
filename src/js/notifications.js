export var notifications = []

export function loadNotifications(){
	var user = JSON.parse(localStorage.getItem('user'));

	var result =  RequestHelper.getRequest('http://localhost:3000/get-notifications?id=' + user.id);
	
	if(!result.success){
		return alert('Something went wrong');
	}

	notifications = result.data;

	console.log(notifications)

	var html = '';
	for(var notification of notifications){
		html += 
		`
		<hr>
		<div class="group-list-items">
		<p style="display: inline-block;">${notification.message}</p>
		<button onclick="Notifications.view('${notification.url}')" type="button" class="button button-success group-button">View</button>
		</div>
		`
	}

	document.getElementById('notifications').innerHTML = html;

}


export function view(url){
	router.navigate(url);
}



