export function setUserForm(){
	var user = JSON.parse(localStorage.getItem('user'));
	var result =  RequestHelper.getRequest('http://localhost:3000/get-user?id=' + user.id);
	if(!result.success){
		return alert('Not Valid user');
	}

	var form = document.forms["userForm"];
	for(var key in result.user){
		if(form[key]){
			if(form[key].value !== undefined){
				form[key].value = result.user[key];				
			}
		}
	}
}



export function editUserProfile() {
	var user = JSON.parse(localStorage.getItem('user'));

	var form = document.forms["userForm"];
	var formData = new FormData();
	formData.append("profile", form['profile'].files[0]);
	formData.append("cover_pricture", form['cover-pricture'].files[0]);
	formData.append("name", form['name'].value);
	formData.append("surname", form['surname'].value);
	formData.append("email", form['email'].value);
	formData.append("birthday", form['birthday'].value);

	var formData = {
		profile:  form['profile'].files[0],
		cover_pricture: form['cover-pricture'].files[0],
		name: form['name'].value,
		surname: form['surname'].value,
		email: form['email'].value,
		birthday:form['birthday'].value,
		id: user.id
	}



	var result = RequestHelper.postRequest('http://localhost:3000/edit-profile', formData);

	if(result.success){
		return alert('Profile updated');
	}else{
		return alert('Error occured');
	}

};


export function editProfilePassword() {
	var user = JSON.parse(localStorage.getItem('user'));

	var form = document.forms["passwordForm"];

	var formData = {
		current_password:  form['current-password'].value,
		new_password: form['new-password'].value,
		repeat_password: form['repeat-password'].value,
		user_id: user.id
	}

	for(var key in formData){
		var value = formData[key];
		if(value.length == 0){
			return alert('Please fill fields');
		}
	}

	if(formData.new_password != formData.repeat_password){
		return alert('Passwords does not match');
	}

	var result = RequestHelper.postRequest('http://localhost:3000/change-password', formData);

	if(!result.success){
		alert(result.error);
	}else{
		alert('Password changed')
	}
};
