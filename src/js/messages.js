export function loadMessages(){
	var user = JSON.parse(localStorage.getItem('user'));
	var id = router._lastRouteResolved.params ? router._lastRouteResolved.params.id : null;



	var result =  RequestHelper.getRequest(`http://localhost:3000/get-messages?from=${user.id}&to=${id}`);

	if(!result.success){
		return alert('Something went wrong');
	}
	var messages = result.messages;

	var html ='';


	for(var message of messages){

		html +=`

		<div class="message ${message.user_from === user.id ? 'user-message' : 'friend-message' }">
		<div class="${message.user_from === user.id ? 'user-image' : 'friend-image' } " >
		<img src="images/user.png" class="post-img">
		</div>
		<p class="${message.user_from === user.id ? 'user-text' : 'friend-text' } ">${message.message}</p>
		</div>
		`
	}

	document.getElementById('messages').innerHTML = html;


}

export function sendMessage() {
	var user = JSON.parse(localStorage.getItem('user'));
	var id = router._lastRouteResolved.params ? router._lastRouteResolved.params.id : null;

	var message = document.getElementById("message").value

	var formData = {
		from: user.id,
		to: id,
		message: message
	}

	var result = RequestHelper.postRequest('http://localhost:3000/add-message', formData);

	if(result.success){
		document.getElementById("message").value = '';
		loadMessages();
	}else{
		return alert('Error occured');
	}

};
