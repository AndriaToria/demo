export function createNewEvent() {
	var user = JSON.parse(localStorage.getItem('user'));

	var form = document.forms["eventForm"];

	var formData = {
		name:  form['name'].value,
		description: form['description'].value,
		date: form['date'].value,
		location: form['location'].value,
		user_id: user.id
	}

	for(let key in formData){
		if(formData[key] == ''){
			return alert('Fill all field')
		}
	}

	

	var result = RequestHelper.postRequest('http://localhost:3000/create-event', formData);

	if(result.success){
		alert('Event created')
		return router.navigate('events');
	}else{
		return alert('Error occured');
	}

};


export function loadEvents(){
	var user = JSON.parse(localStorage.getItem('user'));

	var result =  RequestHelper.getRequest('http://localhost:3000/get-events?id=' + user.id);

	if(!result.success){
		return alert('Something went wrong');
	}

	var events = result.events;

	var html ='';

	events.forEach((elem, index, array) => {
		html +=
		`
		<div class="group-list-items">
		<p style="display: inline-block;">${elem.name}</p>
		<button onclick="router.navigate('event/${elem.id}')" 
		type="button" class="button button-success group-button">Visit Event</button>
		</div>
		`

		if( (array.length -1)  !=  index){
			html += '<hr>'
		}

	})

	document.getElementById("event-list").innerHTML = html;
}




var user = JSON.parse(localStorage.getItem('user'));

export function loadEvent(){
	var id = router._lastRouteResolved.params.id;

	var result =  RequestHelper.getRequest('http://localhost:3000/get-event?id=' + id);

	if(!result.success){
		return alert('Something went wrong');
	}

	var event = result.event;
	var stats =result.stats;

	var html =`
	<div class="main-view">
	<img src="images/cover.jpg" class="event-image">
	<div class="event-info">
	<h1>${event.name}</h1>
	<p><b>Description: </b> ${event.description}</p>
	<p><b>Date:</b> ${event.date}</p>
	<p><b>Location:</b> ${event.location}</p>
	<p><b>Interested: </b> ${stats['interested'] ? stats['interested'] : 0}</p>
	<p><b>Going: </b> ${stats['going'] ? stats['going'] : 0}</p>
	</div>
	</div>
	`;



	document.getElementById("event").innerHTML = html;
}


export function eventPlan() {
	var id = router._lastRouteResolved.params.id;

	var formData = {
		event_id:  id,
		user_id: user.id,
		plan: document.getElementById("plan").value
	}

	var result = RequestHelper.postRequest('http://localhost:3000/add-event-plan', formData);

	if(result.success){
		return alert('Plan saved');
	}else{
		return alert('Error occured');
	}

};

