export function sendRequest(id){
	var currentUser = JSON.parse(localStorage.getItem('user'));

	var formData = {
		user_id: currentUser.id,
		friend_id:id,
	}

	var result = RequestHelper.postRequest('http://localhost:3000/add-friend', formData);

	if(result.success){
		Users.loadUsers();

		return alert("User added");
	}else{
		return alert("Error occured");
	}

}

export function deleteUser(id){
	var currentUser = JSON.parse(localStorage.getItem('user'));

	var formData = {
		user_id: currentUser.id,
		friend_id:id,
	}

	var result = RequestHelper.postRequest('http://localhost:3000/delete-friend', formData);

	if(result.success){
		Friends.loadFriendUsers()

		return alert("User Deleted");
	}else{
		return alert("Error occured");
	}

}