export function checkAuth(){
	
	var user = JSON.parse(localStorage.getItem('user'));
	if(user){
		router.navigate('dashboard')
	}
}


export function showSignUp(argument) {

	
	var logInForm = document.getElementById("login-form");
	var signUpForm = document.getElementById('sign-up-form');

	logInForm.classList.remove("show-element");
	signUpForm.classList.add("show-element");
}

export function showLogin(argument) {
	var logInForm = document.getElementById("login-form");
	var signUpForm = document.getElementById('sign-up-form');

	logInForm.classList.add("show-element");
	signUpForm.classList.remove("show-element");
}