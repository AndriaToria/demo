// Listeners

export function changeDropdownState() {
	var dropdownItems =  document.getElementById("user-dropdown-items");
	if(!dropdownItems.classList.contains("show-element")){
		dropdownItems.classList.add("show-element");
	}else{
		dropdownItems.classList.remove("show-element");;
	}
}

export function removeToken(){
	localStorage.removeItem('user');
	router.navigate('login');
}



export function loggedUserCheck(){
	var user = JSON.parse(localStorage.getItem('user'));
	if(!user){
		router.navigate('login')
	}
}
