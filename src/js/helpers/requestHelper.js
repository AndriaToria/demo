export function  getRequest( url) {
	var http = new XMLHttpRequest();
    http.open( 'GET', url, false);
    http.send();
    return JSON.parse(http.responseText);
}

export function  postRequest( url, params ) {
	var http = new XMLHttpRequest();
	params = Object.keys(params).map(key => key + '=' + params[key]).join('&');
	http.open('POST', url, false);

	http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	http.send(params);
    return JSON.parse(http.responseText);
}


export function postFormData(url, formData){
	var xhr = new XMLHttpRequest();
	xhr.open('POST', url, false);
	// xhr.setRequestHeader('Content-type', 'application/json');
	xhr.send(formData);
}


