export function singIn() {
	var form = document.forms["loginForm"];

	var formData = {
		email:  form['email'].value,
		password: form['password'].value
	}
	
	var query = Object.keys(formData).map(key => key + '=' + formData[key]).join('&');

	var result =  RequestHelper.getRequest('http://localhost:3000/login?' + query);

	if(!result.success){
		alert('incorrect username or password');
	}else{
		localStorage.setItem('user', JSON.stringify(result.user));
		router.navigate('dashboard')
	}

};


export function createAccount() {
	var form = document.forms["createForm"];
	var regexMail = /\S+@\S+\.\S+/;

	var formData = {
		name : form['name'].value,
		surname: form['surname'].value,
		email:form['register-email'].value,
		password: form['register-password'].value,
		repeat_password: form['repeat-password'].value
	}

	if(!regexMail.test(formData.email)){
		return  alert('Not valid mail type');
	}else if(formData.password !== formData.repeat_password){
		return alert('Password does not match');
	}
	for (let key in formData){
		if(formData[key] == ''){
			return alert('Fill fields');
		}
	}


	var result = RequestHelper.postRequest('http://localhost:3000/register', formData);

	if(!result.success){
		alert(result.error);
	}else{
		form['name'].value = '';
		form['surname'].value = '';
		form['register-email'].value = '';
		form['register-password'].value = '';
		form['repeat-password'].value= '';
		LoginHelper.showLogin();
	}

};
