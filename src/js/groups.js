export var groups = [];
export var allGroups = [];



export function loadGroups(){
	var user = JSON.parse(localStorage.getItem('user'));

	var result =  RequestHelper.getRequest('http://localhost:3000/get-groups?id=' + user.id);
	
	if(!result.success){
		return alert('Something went wrong');
	}

	groups = result.data;
	allGroups = groups;

	displayGroups();
}

export function displayGroups(){
	var html = '';

	groups.forEach((group, index,arr) => {
		html += 
		`
		<div class="group-list-items">
		<p style="display: inline-block;">${group.name}</p>
		<button  onclick='Groups.leaveGroup(${group.id})' type="button" class="button button-danger group-button">Leave Group</button>
		<button onclick="router.navigate('group/${group.id}')" type="button" class="button button-success group-button">Visit Group</button>
		</div>
		`
		if(index != arr.length - 1){
			html += '<hr>';
		}
	})

	document.getElementById('groups').innerHTML = html;
}

export function filterGroups(){
	var searchInput = document.getElementById('name').value;


	groups = allGroups.filter((group) => {
		return group.name.indexOf(searchInput) > -1
	})

	if(searchInput.length == 0){
		groups = allGroups;
	}

	displayGroups();

}



export function leaveGroup(group_id){
	var currentUser = JSON.parse(localStorage.getItem('user'));

	var formData = {
		user_id: currentUser.id,
		group_id:group_id,
	}

	var result = RequestHelper.postRequest('http://localhost:3000/leave-group', formData);

	if(result.success){

		loadGroups();

		return alert("You have left group");
	}else{
		return alert("Error occured");
	}
}