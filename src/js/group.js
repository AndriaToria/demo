export function createGroup () {
	var user = JSON.parse(localStorage.getItem('user'));

	var form = document.forms["groupForm"];

	var formData = {
		name:  form['name'].value,
		description: form['description'].value,
		user_id: user.id
	}

	for(let key in formData){
		if(formData[key] ==''){
			return alert('Fill all fields');
		}
	}


	var result = RequestHelper.postRequest('http://localhost:3000/create-group', formData);

	if(result.success){
		alert('Group created');
		return router.navigate('group-list')
	}else{
		return alert('Error occured');
	}

};




export var groupMembers = [];
export var friends = [];
export var groupPosts = [];
export var group = null;

export function addGroupPost () {
	var id = router._lastRouteResolved.params.id;
	
	var user = JSON.parse(localStorage.getItem('user'));


	var formData = {
		user_id: user.id,
		group_id: id,
		post: document.getElementById("group-post").value
	}


	var result = RequestHelper.postRequest('http://localhost:3000/add-group-post', formData);

	if(result.success){
		loadGroupPosts();
	}else{
		return alert('Error occured');
	}

};

export function loadGroupSelect(){
	var html = '';
	console.log(friends)

	for(var friend of friends){
		html +=`
		<option value="${friend.id}">${friend.name} ${friend.surname}</option>
		`
	}
	document.getElementById('invite-friends-select').innerHTML = html;
}

export function loadGroupPosts(){
	
	var user = JSON.parse(localStorage.getItem('user'));
	var id = router._lastRouteResolved.params.id;


	var result =  RequestHelper.getRequest('http://localhost:3000/group-data?id=' + id);
	friends = RequestHelper.getRequest('http://localhost:3000/get-friends?id=' + user.id).data;
	groupMembers = RequestHelper.getRequest('http://localhost:3000/group-members?id=' + id).data;

	groupPosts = result.posts;
	group = result.group;

	displayGroupPosts();
	displayGroupMembers();
    loadGroupSelect();

}

export function displayGroupMembers(){
	
	var html = ''

	for (var groupMember of groupMembers) {
		html +=`
		<tr>
		<td>${groupMember.name} ${groupMember.surname}</td>
		<td>${groupMember.role}</td>
		</tr>

		`
	}

	document.getElementById("group-members").innerHTML = html;
}


export function displayGroupPosts(){
	var html = ``;


	for (var groupPost of groupPosts) {
		html +=`

		<div class="post-section">
		<div class="author">
		<img src="images/user.png" class="post-img">
		</div>
		<div class="context">
		<div class="post-info">
		<p><b>${groupPost.name} ${groupPost.surname}</b></p>
		</div>
		<div class="post-info">
		${groupPost.post}
		</div>
		</div>

		<p style="margin-top: 5px"><b>Comments:</b></p>
		`

		for (var comment of groupPost.comments) {
			html +=`
			<div class="post-info comment">
			<div class="author" >
			<img src="images/user.png" class="post-img">
			</div>
			<p ><b>${comment.name} ${comment.surname}</b> 
			${comment.comment}
			</p>	
			</div>
			`;
		}
		


		html += `
		<div class="post-info">
		<hr>
		<textarea
		id='comment-post-${groupPost.id}' 
		style="width: 99%" 
		rows="2"
		class="textarea"
		placeholder="Reply Post"></textarea>
		<button onclick='Group.reply(${groupPost.id})'  type="button" class="button button-primary reply-button">Reply</button>
		</div>
		</div>
		`
	}

	document.getElementById("group-posts").innerHTML = html;

}

export function reply(id){
	var user = JSON.parse(localStorage.getItem('user'));

	var formData = {
		post_id:id,
		user_id: user.id,
		comment:document.getElementById(`comment-post-${id}`).value
	}

	var result = RequestHelper.postRequest('http://localhost:3000/add-group-comment', formData);

	if(result.success){
		document.getElementById(`comment-post-${id}`).value = ''
		loadGroupPosts();
	}else{
		return alert('Error occured');
	}
}

export function inviteGroup(){
	var id = router._lastRouteResolved.params.id;

	var user = JSON.parse(localStorage.getItem('user'));

	var formData = {
		user_id: document.getElementById('invite-friends-select').value,
		group_id: id,
		inviter: user.name + ' ' + user.surname
	}

	var result = RequestHelper.postRequest('http://localhost:3000/invite-group', formData);

	if(result.success){
		return alert('Invitation Sent');
	}else{
		return alert('Error occured');
	}
}

