var group = `


<div class="content">
	<div class="sections">
		<div class="user-section">

			<div class="card view-options" >
				<h3 style="text-align: center">Add Friend</h3>
				<select id='invite-friends-select' id= 'type' class="input select">
				</select>
				<button onclick='Group.inviteGroup()' type="button" class="button button-success invite-button">Invite</button>


				<div>
					<table>
						<h3 style="text-align: center">Members</h3>
						<thead>
							<tr>
								<th>Fullname</th>
								<th>Role</th> 
							</tr>
						</thead>
						<tbody id='group-members'>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="timeline">
			<!-- New Post -->
			<div class="post-section">
				<h3 style="text-align: center">Add Post</h3>
				<form>
					<textarea 
					id='group-post'
					rows="2"
					class="textarea"
					placeholder="Enter Post"></textarea>
					<button onclick="Group.addGroupPost()" id='add-group-post' type="button" style="margin-top: 10px" class="button button-primary"> Post</button>
				</form>
			</div>


			<!-- News Feed -->





			<!-- Post -->
			<div id='group-posts'>
			</div>

		</div>
	</div>
</div>
`

module.exports = group;