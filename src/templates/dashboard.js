var dashboard =`


<div class="content">
<div class="sections">
<div class="user-section">
<div class="card">
<img src="images/cover.jpg" class="cover-image">

<img src="images/user.png" class="timeline-profile-pic">

<h1 id='user' class="username"></h1>

</div>

<div class="card view-options" style="margin-top: 20px">
<h3 style="text-align: center">View</h3>

<ul>
<li>
<a onclick="router.navigate('group-list')">
<b>Groups</b>

</a>

</li>
<li>
<a onclick="router.navigate('events')" >
<b>Events</b>

</a>
</li>
<li>
<a onclick="router.navigate('polls')">
<b>Polls</b>

</a>
</li>
<li>
<a onclick="router.navigate('friends')" >
<b>Friends</b>
 
</a>
</li>

<li>
<a onclick="router.navigate('all-user')" >
<b>Users</b>

</a>
</li>
<!-- 
<li>
<a href="game.html">
<b>Game</b>

</a>
</li>
-->
</ul>
</div>

<div class="card view-options" style="margin-top: 20px">

<div>
<table>
<h3 style="text-align: center">Coming Birthdays</h3>
<thead>
<tr>
<th>Fullname</th>
<th>Born</th> 
</tr>
</thead>
<tbody id='birtday-body'>

</tbody>
</table>

</div>


</div>


</div>

<div class="timeline">
<!-- New Post -->
<div class="post-section">
<h3 style="text-align: center">Add Post</h3>
<form>
<textarea 
rows="2"
id='post'
class="textarea"
placeholder="Enter Post"></textarea>
<p id='tag-names' for="taged-friends">Tags: </p>
<select id='tags' id= 'type' class="input">
</select>


<button onclick="Dashboard.addTagFunction()" id='add-tag' type="button" style="margin-top: 10px; margin-right: 5px" class="button button-success"> Add Tagged Friend </button>
<button onclick="Dashboard.addPost()" id='add-post' type="button" style="margin-top: 10px" class="button button-primary"> Post</button>
</form>
</div>


<!-- News Feed -->




<div id='posts'>

</div>



</div>
</div>
</div>

`

module.exports =dashboard