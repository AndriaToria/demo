var groupForm = `


	<div class="content">
		<div class="section">
			<h1 style="padding-left: 5px"><i>Create Group</i></h1>
			<form id='groupForm' class="user-form">
				<div class="input-div">
					<p for="name">Name: </p>
					<input id="name" type="text" name="name" placeholder="Please Enter Name" class="input">
				</div>

				<div class="input-div">
					<p for="description">Description: </p>
					<input id="description" type="text" name="description" placeholder="Please Enter Description" class="input">
				</div>

				<button onclick="Group.createGroup()" id='create-group' type="button" class="button button-center button-primary edit-profile-button" >Save</button>
			</form>

		</div>
	</div>

`


module.exports = groupForm