var auth = `
<div id="login-template" >
        <div class="login-register">
            <form id="sign-up-form" name ='createForm' class="form">
                <h1 class="header">Register</h1>

                <div class="input-div">
                    <label for="name">Name: </label>
                    <input id="name" type="text" name="name" placeholder="Please Enter Name" class="input">
                </div>

                <div class="input-div">
                    <label for="surname">Surname: </label>
                    <input id="surname" type="text" name="surname" placeholder="Please Enter Surname" class="input">
                </div>


                <div class="input-div">
                    <label for="email">Email: </label>
                    <input id="register-email" type="email" name="email" placeholder="Please Enter Email" class="input">
                </div>

                <div class="input-div">
                    <label for="password">Password: </label>
                    <input id="register-password" type="password" name="password" placeholder="Please Enter Password" class="input">
                </div>

                <div class="input-div">
                    <label for="repeat-password">Repeat Password: </label>
                    <input id="repeat-password" type="password" name="repeat-password" placeholder="Please Repeat Password" class="input">
                </div>


                <button onclick="Authentication.createAccount()" id='create-account' type="button" class="button button-center button-success" >Sign Up</button>
                <button onclick="LoginHelper.showLogin()" id="login" type="button" class="button button-center button-primary" style="margin-top: 5px">Log In</button>
            </form>
            <form id="login-form" name='loginForm' class="form show-element">
                <h1 class="header">Login</h1>
                <div class="input-div">
                    <label for="email">Email: </label>
                    <input id="email" type="email" name="email" placeholder="Please Enter Email" class="input">
                </div>

                <div class="input-div">
                    <label for="password">Password: </label>
                    <input id="password" type="password" name="password" placeholder="Please Enter Password" class="input">
                </div>

                <button onclick="Authentication.singIn()" id='sign-in' type="button" class="button button-center button-primary">Sign In</button>
                <button onclick="LoginHelper.showSignUp()" id="sign-up" type="button" class="button button-center button-success" style="margin-top: 10px">Sign Up</button>
            </form>
        </div>


`;

module.exports = auth