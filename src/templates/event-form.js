var eventForm = `

<div class="content">
<div class="section">
<h1 style="padding-left: 5px"><i>Create Event</i></h1>
<form id='eventForm' class="user-form">
<div class="input-div">
<p for="name">Name: </p>
<input id="name" type="text" name="name" placeholder="Please Enter Name" class="input">
</div>

<div class="input-div">
<p for="surname">Description:</p>
<textarea 
id='description'
rows="4"
class="textarea"
placeholder="Event Description"></textarea>
</div>

<div class="input-div">
<p for="birthday">Event Date: </p>
<input id='date' id="event_date" type="datetime-local" name="event_date" placeholder="Please Enter Event Date" class="input">
</div>

<div class="input-div">
<p for="name">Location: </p>
<input id='location' id="location" type="text" name="location" placeholder="Please Enter Location" class="input">
</div>


<button onclick="Events.createNewEvent()" id='create-event' type="button" class="button button-center button-primary edit-profile-button">Save</button>
</form>

</div>
</div>



`


module.exports = eventForm