var groupList = `

	<div class="content">
		<div class="group-bar">
			<div class="group-items">
				<h3 style="text-align: center">Group Actions</h3>
				<input  onchange="Groups.filterGroups()" id='name' type="text" name="name" placeholder="Search Group" class="input group-input">
				<button id='create-group' onclick="router.navigate('group-form')"  type="button" class="button button-primary group-add">Create Group</button>
			</div>
		</div>


		<div id='groups' class="group-bar">


		</div>
	</div>


`

module.exports =groupList