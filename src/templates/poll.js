var poll = `
	

	<div class="content">
		<div class="sections">
			
			<div class="polls">
				<!-- New Post -->
				<div class="post-section">
					<h3 style="text-align: center">Add Poll</h3>
					<form id='pollForm'>
						<div class="input-div">
							<label for="question">Question: </label>
							<input id='question' type="text" name="question" class="input" placeholder="Enter Question">
						</div>
						<div class="input-div">
							<label for="option1">Option 1: </label>
							<input id='answer1' type="text" name="option1" class="input" placeholder="Enter Option 1">

						</div>
						<div class="input-div">
							<label for="option2">Option 2: </label>
							<input id='answer2' type="text" name="option2" class="input" placeholder="Enter Option 2">
						</div>
						<button onclick="Polls.addPoll()" id='add-poll' type="button" style="margin-top: 10px" class="button button-primary">Add Poll</button>
					</form>
				</div>


				
				<div id='all-poll'>
					
					<!-- Question -->
					<!-- <div class="post-section">
						<div class="post-info">
							<p><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</b></p>
						</div>
						<div class="post-info">
							<input type="radio" name="answer" value="option1">Lorem ipsum dolor sit amet, consectetur adipiscing elit
							<br>
							<input type="radio" name="answer" value="option2">Lorem ipsum dolor sit amet, consectetur adipiscing elit
							<br>
							<button type="button" style="margin-top: 10px; float: right;" class="button button-success">Vote</button>

						</div>
					</div>
				-->
				

			</div>


			
		</div>
	</div>

`

module.exports = poll