var editProfile = `

	<div class="content">
		<div class="section">
			<h1 style="padding-left: 5px"><i>Edit Profile</i></h1>
			<form id='userForm' class="user-form">
				<div class="input-div">
					<p for="name">Name: </p>
					<input id="name" type="text" name="name" placeholder="Please Enter Name" class="input">
				</div>

				<div class="input-div">
					<p for="surname">Surname: </p>
					<input id="surname" type="text" name="surname" placeholder="Please Enter Surname" class="input">
				</div>


				<div class="input-div">
					<p for="email">Email: </p>
					<input id="email" type="email" name="email" placeholder="Please Enter Email" class="input">
				</div>

				<div class="input-div">
					<p for="birthday">Birthday: </p>
					<input id="birthday" type="date" name="birthday" placeholder="Please Enter Birthday" class="input">
				</div>


				<button onclick="Profile.editUserProfile()" id='edit-profile' type="button" class="button button-center button-primary edit-profile-button" >Edit Profile</button>
			</form>

		</div>

		<div class="section">
			<h1 style="padding-left: 5px"><i>Change Password</i></h1>
			<form id='passwordForm' class="user-form">
				<div class="input-div">
					<p for="current-password">Current Password: </p>
					<input id="current-password" type="password" name="current-password" placeholder="Please Current Password" class="input">
				</div>

				<div class="input-div">
					<p for="new-password">New Password: </p>
					<input id="new-password" type="password" name="new-password" placeholder="Please Enter New Password" class="input">
				</div>


				<div class="input-div">
					<p for="repeat-password">Repeat Password: </p>
					<input id="repeat-password" type="password" name="repeat-password" placeholder="Please Repeat Password" class="input">
				</div>

				<button onclick="Profile.editProfilePassword()" id='edit-password' type="button" class="button button-center button-success edit-password-button" >Change Password</button>
			</form>

		</div>
	</div>

`

module.exports = editProfile;