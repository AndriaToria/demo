var commonHeader = `

    <div class="head">

        <div class="navigation-actions">
            <ul>
                <li class="navigation-item">
                    <a onclick="router.navigate('dashboard')" >
                        <img src="images/home.png" class="navigation-icon"> 
                        <p>Home</p>
                    </a>
                </li>

                <li class="navigation-item">
                    <a  onclick="router.navigate('notifications')" >
                        <img src="images/notification.png" class="navigation-icon"> 
                        <p>Notifications</p>
                    </a>
                </li>
            </ul>
        </div>


        <div class="user-bar">
            <div class="user-dropdown">
                <div class="dropdown-head" id="user-dropdown-head" onclick="DashboardHelper.changeDropdownState()">
                    <img src="images/user.png" class="user-icon"> 
                </div>
                <div class="dropdown-items" id="user-dropdown-items">
                    <ul>
                        <li>
                            <a onclick="router.navigate('profile')">Profile</a>
                        </li>
                        <li>
                            <a onclick="router.navigate('edit-profile')">Edit Profile</a>
                        </li>
                        <li>
                            <a id='logout' onclick="DashboardHelper.removeToken()">Log Out</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


`

module.exports = commonHeader