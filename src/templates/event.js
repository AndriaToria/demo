var event = `
	<div class="content">
		<div class="sections">

			<div id='event' class="event">
				
			</div>

			<div class="event">
				<div class="main-view">
					<div class="event-action">
						<select id='plan' class="input" style="margin-top: 20px">
							<option value="interested">Interested</option>
							<option value="going">Going</option>
						</select>
						<button onclick="Events.eventPlan()" id='save' type="button" class="button button-primary event-button">Save</button>
					</div>
				</div>
			</div>

		</div>
	</div>

`

module.exports = event;