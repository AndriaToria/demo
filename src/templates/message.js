var message = `

	<div class="content">
		<div class="message-bar">
			<h1 style="text-align: center;">Chat</h1>
			<hr>
			
			
			<div id='messages' class="messages">
				
			</div>

			<div class="send-message">
				<textarea 
				id='message'
				rows="3"
				class="textarea message-inbox"
				placeholder="Enter Message"></textarea>

				<button onclick='Messages.sendMessage()' id='send' type="button" class="button button-primary send-message-button">Send</button>

			</div>

		</div>
	</div>

`



module.exports = message;