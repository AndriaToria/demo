const mysql = require('mysql');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require('fs');
const path = require("path");
var app = express();
app.use(cors());


app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));
app.use('/js', express.static(__dirname + '/public/js'));


// -----------------------------------------DB CONFIG -------------------------------------
var mysqlCon = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '1234',
	database: 'social'
});
mysqlCon.connect((err) => {
	if(!err){
		console.log('DB connected');
	}else{
		console.log(JSON.stringify(err));
	}
});

app.listen(3000, () => {
	console.log('Server Started');
});

// HELPERS

function parseResult(res){
	return JSON.parse(JSON.stringify(res))[0];
}

function setNotification(req,res, to, message,url){
	var insertQuery = 'INSERT INTO notifications SET ?'
	mysqlCon.query(insertQuery, {
		to: to,
		message: message,
		url: url
	}, (error, results, fields) => {
		if (error){
			res.send({
				success: false,
				error: error
			})
		}else{
			res.send({
				success: true,
			})	
		}
	});
}


function addMemberInGroup(req,res, group_id, user_id){
	var insertQuery = 'INSERT INTO group_members SET ?'
	mysqlCon.query(insertQuery, {
		group_id: group_id,
		user_id: user_id
	}, (error, results, fields) => {
		if (error){
			res.send({
				success: false,
				error: error
			})
		}else{
			res.send({
				success:true
			})
		}
	});
}


function currentDate(){
	var date = new Date();
	var localTime = date.toLocaleDateString();
	return localTime.split('/').reverse().join('/');
}


// ROUTES


app.post('/change-password', (req, res) => {
	var sql = `SELECT COUNT(*) as count from users where id = ? and password = ?`;
	mysqlCon.query(sql, [req.body.user_id, req.body.current_password] ,(err, rows, fields) => {
		var count =  parseResult(rows).count;
		if(count){
			var insertQuery = 'UPDATE users SET password = ? where id = ?'
			mysqlCon.query(insertQuery, [req.body.new_password, req.body.user_id], (error, results, fields) => {
				if (error){
					res.send({
						success: false,
						error: error
					})
				}else{
					res.send({
						success:true,
					})
				}
			});
		}else{
			res.send({
				success:false,
				error: 'Inccorect password'
			})
		}
	});
});


app.post('/edit-profile', async (req, res) => {

	var sql = `SELECT COUNT(*) as count from users where id = ?`;
	mysqlCon.query(sql, [req.body.id] ,(err, rows, fields) => {
		if(!err){
			var result = parseResult(rows);
			if(result.count){
				var insertQuery = 'UPDATE users SET birthday = ?, ' +
				' email = ?, name = ?,  surname = ? ' +
				' where id = ?'
				mysqlCon.query(insertQuery, [
					req.body.birthday,
					req.body.email,
					req.body.name,
					req.body.surname,
					req.body.id
					], (error, results, fields) => {
						if (error){
							res.send({
								success: false,
								error: error
							})
						}else{
							res.send({
								success:true,
							})
						}
					});

			}


		}else{
			res.send({
				success: false,
				error:err
			});
		}
	});

});




app.post('/register', (req, res) => {
	var sql = `SELECT COUNT(*) as count from users where email = ?`;
	mysqlCon.query(sql, [req.body.email] ,(err, rows, fields) => {
		if(!err){
			var result = parseResult(rows);
			if(result.count){
				res.send({
					success: false,
					error: 'User with such email already exists'
				});
			}else{
				var insertQuery = 'INSERT INTO users SET ?'
				mysqlCon.query(insertQuery, {
					name: req.body.name,
					surname: req.body.surname,
					email: req.body.email,
					password: req.body.password

				}, (error, results, fields) => {
					if (error){
						res.send({
							success: false,
							error: error
						})
					}else{
						res.send({
							success:true,
						})
					}
				});
			}
		}else{
			res.send({
				success: false,
				error:err
			});
		}
	});
});



app.get('/login', (req, res) => {
	var email = req.query.email;
	var password = req.query.password;
	var sql = `SELECT COUNT(*) as count FROM users where email = ? and password = ?`
	mysqlCon.query(sql, [email, password] ,(err, rows, fields) => {
		if(!err){
			var count =  parseResult(rows).count;

			if(count){
				var sql = `SELECT id, name, surname, email, birthday, profile_pic, cover_pic 
				FROM users where email = ?`
				mysqlCon.query(sql, [email] ,(error, result) => {
					if(error){
						res.send(error);
					}else{
						var data = {
							success:true,
							user: parseResult(result)
						};
						res.send(data);
					}
				});
			}else{
				return res.send({
					success:false
				})
			}

		}else{
			console.log(JSON.stringify(err));
		}
	})
});



app.get('/get-user', (req, res) => {
	var sql = `SELECT id, name, surname, email, birthday
	FROM users where id = ?`
	mysqlCon.query(sql, [req.query.id] ,(err, result, fields) => {
		if(!err){
			var data = {
				success:true,
				user: parseResult(result)
			};
			res.send(data)
		}else{
			return res.send({
				success:false
			})
		}
	});
});




app.post('/create-group', (req, res) => {
	var insertQuery = 'INSERT INTO groups SET ?'
	mysqlCon.query(insertQuery, {
		name: req.body.name,
		description: req.body.description,
		type: req.body.type,
		created_by: req.body.user_id
	}, (error, results, fields) => {
		if (error){
			res.send({
				success: false,
				error: error
			})
		}else{
			addMemberInGroup(req,res,results.insertId, req.body.user_id);
		}
	});
});





app.post('/add-poll', (req, res) => {
	var insertQuery = 'INSERT INTO polls SET ?'
	mysqlCon.query(insertQuery, {
		question: req.body.question,
		answer1: req.body.answer1,
		answer2: req.body.answer2,
	}, (error, results, fields) => {
		if (error){
			res.send({
				success: false,
				error: error
			})
		}else{
			var getPollQuery = 'SELECT * from polls where id = ?'
			var id = results.insertId;

			mysqlCon.query(getPollQuery, [id] ,(err, poll) => {
				if(!err){
					var data = {
						success:true,
						poll: parseResult(poll)
					};
					res.send(data)
				}else{
					return res.send({
						success:false
					})
				}
			});
		}
	});
});


app.get('/get-polls', (req, res) => {
	var sql = `SELECT * from polls`
	mysqlCon.query(sql, [] ,(err, result, fields) => {
		if(!err){
			var data = {
				success:true,
				data: result
			};
			res.send(data)
		}else{
			return res.send({
				success:false
			})
		}
	});
});



app.post('/answer-poll', (req, res) => {
	var insertQuery = 'INSERT INTO poll_answers SET ?'
	mysqlCon.query(insertQuery, {
		poll_id: req.body.poll_id,
		answer: req.body.answer,
	}, (error, results, fields) => {
		if (error){
			res.send({
				success: false,
				error: error
			})
		}else{
			var getStatQuery = 
			'select answer, count(answer) as total ' +
			'from poll_answers ' + 
			'where poll_id = ? ' +
			'group by answer; '

			mysqlCon.query(getStatQuery, [req.body.poll_id] ,(err, stats) => {
				if(!err){
					var data = {
						success:true,
						stats: stats
					};
					res.send(data)
				}else{
					return res.send({
						success:false
					})
				}
			});

		}
	});
});


app.get('/get-users', (req, res) => {
	var sql = `
	SELECT id, name, surname from users where id != ? and id not in 
	(select friend_id from friends where user_id = ?)
	`
	mysqlCon.query(sql, [req.query.id,req.query.id] ,(err, result, fields) => {
		if(!err){
			var data = {
				success:true,
				data: result
			};
			res.send(data)
		}else{
			return res.send({
				success:false
			})
		}
	});
});



app.post('/add-friend', (req, res) => {
	var sql = `SELECT id,name, surname from users where id = ?`
	mysqlCon.query(sql, [req.body.user_id] ,(err, user) => {
		user = parseResult(user);
		if(!err){
			var insertQuery = 'INSERT INTO friends SET ?'
			mysqlCon.query(insertQuery, {
				user_id: req.body.user_id,
				friend_id: req.body.friend_id,
			}, (error, friendsResults) => {
				if (error){
					res.send({
						success: false,
						error: error
					})
				}else{
					var message = user.name + ' ' + user.surname + ' has added you as friend';
					var url = `profile/${user.id}`;
					setNotification(req,res,req.body.friend_id, message,url)
				}
			});
		}else{
			return res.send({
				success:false
			})
		}
	});
});



app.get('/get-friends', (req, res) => {
	var sql = `
	SELECT id, name, surname from users where id != ? and id in 
	(select friend_id from friends where user_id = ?)
	`
	mysqlCon.query(sql, [req.query.id,req.query.id] ,(err, result, fields) => {
		if(!err){
			var data = {
				success:true,
				data: result
			};
			res.send(data)
		}else{
			return res.send({
				success:false
			})
		}
	});
});


app.post('/delete-friend', (req, res) => {
	var sql = `SELECT id,name, surname from users where id = ?`
	mysqlCon.query(sql, [req.body.user_id] ,(err, user) => {
		user = parseResult(user);
		if(!err){
			var insertQuery = 'Delete from friends where user_id = ? and friend_id = ?'
			mysqlCon.query(insertQuery, [
				req.body.user_id, req.body.friend_id,
				], (error, friendsResults) => {
					if (error){
						res.send({
							success: false,
							error: error
						})
					}else{
						var message = user.name + ' ' + user.surname + ' has removed from his friends';
						var url = `profile/${user.id}`;
						setNotification(req,res, req.body.friend_id, message,url)
					}
				});
		}else{
			return res.send({
				success:false
			})
		}
	});
});


app.get('/get-notifications', (req, res) => {
	var sql = `SELECT * from notifications where notifications.to = ? `
	mysqlCon.query(sql, [parseInt(req.query.id)] ,(err, result, fields) => {
		if(!err){
			var data = {
				success:true,
				data: result
			};
			res.send(data)
		}else{
			console.log(err)
			return res.send({
				success:false
			})
		}
	});
});


app.get('/get-groups', (req, res) => {
	var sql = `SELECT groups.* from groups inner join group_members on group_members.group_id = groups.id
	where group_members.user_id = ?`
	mysqlCon.query(sql, [parseInt(req.query.id)] ,(err, result, fields) => {
		if(!err){
			var data = {
				success:true,
				data: result
			};
			res.send(data)
		}else{
			console.log(err)
			return res.send({
				success:false
			})
		}
	});
});


app.post('/leave-group', (req, res) => {
	var sql = `Delete from group_members where user_id = ? and group_id = ?`
	mysqlCon.query(sql, [req.body.user_id, req.body.group_id] ,(err, user) => {
		if(!err){
			return res.send({
				success:true
			})
		}else{
			return res.send({
				success:false
			})
		}
	});
});



app.get('/dashboard-data', async (req, res) => {
	var user_id = req.query.id;

	var sql = `SELECT name, surname, birthday, email from users where id = ? `
	mysqlCon.query(sql, [user_id] ,(err, user) => {
		if(!err){
			var sql = `SELECT id,name, surname, birthday, email from users where users.id in (select friend_id from friends where user_id = ?)`
			mysqlCon.query(sql, [user_id] ,(err1, friends) => {
				if(!err1){
					var sql = `
					SELECT user_posts.*, users.name, users.surname, post_tags.user_ids as taggedUsers
					from user_posts 
					inner join users on users.id =user_posts.user_id 
					left join post_tags on post_tags.post_id = user_posts.id
					where user_id = ? 
					order by id desc`
					mysqlCon.query(sql, [user_id] ,(err2, posts) => {
						if(!err2){
							var sql = `select user_post_comments.*, users.name, users.surname 
							from user_post_comments  
							inner join user_posts on user_posts.id = user_post_comments.post_id 
							left join users on user_post_comments.user_id = users.id
							where user_posts.user_id = ?`
							mysqlCon.query(sql, [user_id] ,(err3, comments) => {
								if(!err3){

									for (var post of posts) {
										post.comments = [];
										for (var comment of comments) {
											if(post.id == comment.post_id){
												post.comments.push(comment);
											}
										}
									}
									return res.send({
										success:true,
										data:{
											friends:friends,
											posts:posts,
											user: parseResult(user)
										}
									})

								}else{
									console.log(err3);
									return res.send({
										success:false
									})
								}
							});
						}else{
							console.log(err2)
							return res.send({
								success:false
							})
						}
					});
				}else{
					console.log(err1);
					return res.send({
						success:false
					})
				}
			});
		}else{
			return res.send({
				success:false
			})
		}
	});

});




app.post('/add-post', (req, res) => {
	
	var insertQuery = 'INSERT INTO user_posts SET ?'
	mysqlCon.query(insertQuery, {
		user_id: req.body.user_id,
		content: req.body.post,
		created_at: currentDate()
	}, (error, newPost, fields) => {
		if (error){
			res.send({
				success: false,
				error: error
			})
		}else{
			if(req.body.tagIDs){
				var newInsert = 'INSERT INTO post_tags SET ?'
				mysqlCon.query(newInsert, {
					post_id: newPost.insertId,
					user_ids: req.body.tagIDs,
				}, (err, tags, fields) => {
					if (error){
						res.send({
							success: false,
							error: err
						})
					}else{
						res.send({
							success: true,
						})	
					}
				});
			}else{
				res.send({
					success: true,
				})	
			}
			
		}
	});
});


app.post('/add-comment', (req, res) => {
	var newInsert = 'INSERT INTO user_post_comments SET ?'
	mysqlCon.query(newInsert, {
		post_id: req.body.post_id,
		user_id: req.body.user_id,
		comment: req.body.comment,
		created_at: currentDate()
	}, (err, tags, fields) => {
		if (err){
			res.send({
				success: false,
				error: err
			})
		}else{
			res.send({
				success: true,
			})	
		}
	});
});



app.get('/all-users', (req, res) => {
	var newInsert = 'select id, name, surname from users'
	mysqlCon.query(newInsert,[], (err, tags, fields) => {
		if (err){
			res.send({
				success: false,
				error: err
			})
		}else{
			res.send({
				success: true,
				data: tags
			})	
		}
	});
});


app.get('/get-messages', (req, res) => {
	var newInsert = `
	select user_from, user_to, messages.message
	from messages
	where (messages.user_from = ? and messages.user_to = ?) or 
	(messages.user_from = ? and messages.user_to = ?)
	order by messages.id asc
	`
	mysqlCon.query(newInsert,[
		req.query.from,
		req.query.to,
		req.query.to,
		req.query.from
		], (err, messages, fields) => {
			if (err){
				res.send({
					success: false,
					error: err
				})
			}else{
				res.send({
					success: true,
					messages: messages
				})	
			}
		});
});


app.post('/add-message', (req, res) => {
	var newInsert = 'INSERT INTO messages SET ?'
	mysqlCon.query(newInsert, {
		user_from: req.body.from,
		user_to: req.body.to,
		message: req.body.message,
	}, (err, message, fields) => {
		if (err){
			res.send({
				success: false,
				error: err
			})
		}else{
			res.send({
				success: true,
			})	
		}
	});
});



app.post('/create-event', (req, res) => {
	var newInsert = 'INSERT INTO events SET ?'
	mysqlCon.query(newInsert, {
		name: req.body.name,
		description: req.body.description,
		date: req.body.date,
		location: req.body.location
	}, (err, event, fields) => {
		if (err){
			res.send({
				success: false,
				error: err
			})
		}else{
			var newInsert = 'INSERT INTO user_events SET ?'
			mysqlCon.query(newInsert, {
				user_id: req.body.user_id,
				event_id: event.insertId,
			}, (err, result, fields) => {
				if (err){
					res.send({
						success: false,
						error: err
					})
				}else{
					res.send({
						success: true,
					})	
				}
			});
		}
	});
});


app.get('/get-events', (req, res) => {
	var newInsert = `
	select *
	from events
	`
	mysqlCon.query(newInsert,[], (err, events, fields) => {
		if (err){
			res.send({
				success: false,
				error: err
			})
		}else{
			res.send({
				success: true,
				events: events
			})	
		}
	});
});


app.get('/get-event', (req, res) => {
	var newInsert = `
	select *
	from events
	where events.id = ? 
	`
	mysqlCon.query(newInsert,[
		req.query.id
		], (err, event, fields) => {
			if (err){
				res.send({
					success: false,
					error: err
				})
			}else{
				var newInsert = `
				select plan, count(plan) as count
				from user_events
				where event_id = ?
				group by event_id, plan
				`
				mysqlCon.query(newInsert,[req.query.id], (err, stats, fields) => {
					data = {};
					stats.forEach((stat) => {
						data[stat['plan']] = stat['count']
					})


					if (err){
						res.send({
							success: false,
							error: err
						})
					}else{
						res.send({
							success: true,
							event: parseResult(event),
							stats: data
						})	
					}
				});
			}
		});
});


app.post('/add-event-plan', (req, res) => {
	var newInsert = `
	SELECT * from user_events where event_id = ? and user_id = ?
	`
	mysqlCon.query(newInsert,[
		req.body.event_id,
		req.body.user_id
		], (err, event, fields) => {
			if (err){
				res.send({
					success: false,
					error: err
				})
			}else{
				if(event.length > 0){
					var insertQuery = 'UPDATE user_events SET plan = ? where user_id = ? and event_id = ?'
					mysqlCon.query(insertQuery, 
						[
						req.body.plan, 
						req.body.user_id,
						req.body.event_id
						],
						(error, results, fields) => {
							if (error){
								res.send({
									success: false,
									error: error
								})
							}else{
								res.send({
									success:true,
								})
							}
						});
				}else{
					var newInsert = 'INSERT INTO user_events SET ?'
					mysqlCon.query(newInsert, {
						event_id: req.body.event_id,
						user_id: req.body.user_id,
						plan: req.body.plan,
					}, (err, message, fields) => {
						if (err){
							res.send({
								success: false,
								error: err
							})
						}else{
							res.send({
								success: true,
							})	
						}
					});
				}
			}
		});
});


app.post('/add-group-post', (req, res) => {
	
	var insertQuery = 'INSERT INTO group_posts SET ?'
	mysqlCon.query(insertQuery, {
		user_id: req.body.user_id,
		group_id: req.body.group_id,
		post: req.body.post
	}, (error, newPost, fields) => {
		if (error){
			res.send({
				success: false,
				error: error
			})
		}else{
			res.send({
				success: true,
			})	
		}
	});
});


app.post('/add-group-comment', (req, res) => {
	
	var insertQuery = 'INSERT INTO group_post_comments SET ?'
	mysqlCon.query(insertQuery, {
		user_id: req.body.user_id,
		post_id: req.body.post_id,
		comment: req.body.comment
	}, (error, newPost, fields) => {
		if (error){
			res.send({
				success: false,
				error: error
			})
		}else{
			res.send({
				success: true,
			})	
		}
	});
});


app.get('/group-data', (req, res) => {
	var query = `
	select *
	from groups
	inner join users on users.id = groups.created_by
	where groups.id = ? 
	`
	mysqlCon.query(query,[req.query.id], (err, group, fields) => {
		if (err){
			res.send({
				success: false,
				error: err
			})
		}else{
			group = parseResult(group);


			var postQuery = `
			select group_posts.* , users.name, users.surname
			from group_posts
			inner join users on users.id = group_posts.user_id
			where group_posts.group_id = ? 
			order by group_posts.id desc
			`
			mysqlCon.query(postQuery,[req.query.id], (err, posts, fields) => {
				if (err){
					res.send({
						success: false,
						error: err
					})
				}else{

					var commentQuery = `
					select group_post_comments.*, users.name, users.surname 
					from group_post_comments
					inner join users where users.id = group_post_comments.user_id 
					`
					mysqlCon.query(commentQuery,[], (err, comments, fields) => {
						if (err){
							res.send({
								success: false,
								error: err
							})
						}else{

							for (var post of posts) {
								post.comments = [];
								for (var comment of comments) {
									if(post.id == comment.post_id){
										post.comments.push(comment);
									}
								}
							}
							res.send({
								success: true,
								posts: posts,
								group: group
							})	
						}
					});


					
				}
			});
		}
	});
});


app.post('/invite-group', (req, res) => {
	var newInsert = `
	SELECT * from group_members where user_id = ? and group_id = ?`
	mysqlCon.query(newInsert,[
		req.body.user_id,
		req.body.group_id
		], (err, event, fields) => {
			if (err){
				res.send({
					success: false,
					error: err
				})
			}else{
				if(event.length == 0){
					var insertQuery = 'INSERT INTO group_members SET ?'
					mysqlCon.query(insertQuery, {
						user_id: req.body.user_id,
						group_id: req.body.group_id
					}, (error, newMember, fields) => {
						if (error){
							res.send({
								success: false,
								error: error
							})
						}else{
							var message = req.body.inviter + ' has added you at group';
							var url = `group/${req.body.group_id}`;
							setNotification(req,res, req.body.user_id, message,url)
						}
					});
				}else{
					res.send({
						success: true,
					})	
				}
				
			}
		});


});


app.get('/group-members', (req, res) => {
	var newInsert = `
	select users.name, users.surname, 
	(CASE WHEN groups.created_by = users.id then 'Admin' else 'Member' end ) as role
	from users
	inner join group_members on group_members.user_id = users.id
	inner join groups on group_members.group_id = groups.id
	where groups.id = ? 
	`
	mysqlCon.query(newInsert,[req.query.id], (err, users, fields) => {
		if (err){
			res.send({
				success: false,
				error: err
			})
		}else{
			res.send({
				success: true,
				data: users
			})	
		}
	});
});

